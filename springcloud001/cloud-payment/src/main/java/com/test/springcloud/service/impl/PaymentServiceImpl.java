package com.test.springcloud.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.springcloud.dao.PaymentMapper;
import com.test.springcloud.entities.Payment;
import org.redisson.Redisson;
import org.springframework.stereotype.Service;
import com.test.springcloud.service.PaymentService;

/**
 * @ClassName PaymentServiceImpl
 * @Author hepengfei
 * @Desc
 * @Date 2024/1/5 下午8:02
 */
@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements PaymentService {




}
