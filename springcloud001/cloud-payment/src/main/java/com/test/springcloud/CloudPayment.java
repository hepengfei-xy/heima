package com.test.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName CloudPayment
 * @Author hepengfei
 * @Desc
 * @Date 2024/1/5 下午8:11
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = {"com.test"})
@MapperScan(basePackages = {"com.test.**.dao"})
public class CloudPayment {

    public static void main(String[] args) {
        SpringApplication.run(CloudPayment.class,args);
    }

}
