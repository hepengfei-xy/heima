package com.test.springcloud.controller;

import com.test.springcloud.entities.CommonResult;
import com.test.springcloud.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.test.springcloud.service.PaymentService;

import javax.annotation.Resource;

/**
 * @ClassName PaymentController
 * @Author hepengfei
 * @Desc
 * @Date 2024/1/5 下午8:03
 */
@RequestMapping("/payment")
@RestController
@Slf4j
public class PaymentController {

    @Resource
    private PaymentService paymentService;
    @PostMapping(value = "")
    public CommonResult create(@RequestBody Payment payment) {
        try {
            paymentService.save(payment);
            log.info("插入完成");
            return new CommonResult(200, "插入成功", payment);
        } catch (Exception e) {
            return new CommonResult(500, "插入失败", null);
        }
    }
    @GetMapping(value = "/{id}")
    public CommonResult query(@PathVariable("id") Long id) {
        log.info("进来了吗");
        Payment payment = paymentService.getById(id);
        if (payment != null) {
            return new CommonResult(200, "查询成功", payment);
        }
        return new CommonResult(500, "查询失败", null);
    }

}
