package com.test.springcloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.springcloud.entities.Payment;

/**
 * @ClassName PaymentMapper
 * @Author hepengfei
 * @Desc
 * @Date 2024/1/5 下午7:59
 */
public interface PaymentMapper extends BaseMapper<Payment> {
}
