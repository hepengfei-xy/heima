package com.test.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.springcloud.entities.Payment;

/**
 * @ClassName PaymentService
 * @Author hepengfei
 * @Desc
 * @Date 2024/1/5 下午8:00
 */
public interface PaymentService extends IService<Payment> {
}
