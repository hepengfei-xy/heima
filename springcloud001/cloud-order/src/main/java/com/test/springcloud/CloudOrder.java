package com.test.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName CloudOrder
 * @Author hepengfei
 * @Desc
 * @Date 2024/1/6 下午5:43
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = {"com.test"})
@MapperScan(basePackages = {"com.test.**.dao"})
public class CloudOrder {

    public static void main(String[] args) {
        SpringApplication.run(CloudOrder.class,args);
    }
}
